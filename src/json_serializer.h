#ifndef JSON_SERIALIZER_H
#define JSON_SERIALIZER_H

#include <ArduinoJson.h>

class JSONSerializeableKVStore
{
public:
    JSONSerializeableKVStore() : _doc(1024) {}

    template <typename T>
    void addKeyValue(const String &key, const T &value)
    {
        _doc[key] = value;
    }

    template <typename T>
    void getValue(const String &key, T &value)
    {
        value = _doc[key].as<T>();
    }

    String serialize()
    {
        String output;
        serializeJson(_doc, output);
        return output;
    }

private:
    DynamicJsonDocument _doc;
};

#endif // JSON_SERIALIZER_H