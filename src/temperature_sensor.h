#ifndef TEMPERATURE_SENSOR_H
#define TEMPERATURE_SENSOR_H

#include <OneWire.h>
#include <DallasTemperature.h>

class TemperatureSensor
{
public:
    TemperatureSensor(int pinNumber) : _lastTemperature(-2.0)
    {
        _pinNumber = pinNumber;
        _oneWire = OneWire(pinNumber);
        _sensor = DallasTemperature(&_oneWire);
    };
    float getTemperature()
    {
        Serial.print(" Requesting temperatures...");
        _sensor.requestTemperatures();
        Serial.print("Temperature is: ");
        _lastTemperature = _sensor.getTempCByIndex(0);

        return _lastTemperature;
    }

    JSONSerializeableKVStore read()
    {
        JSONSerializeableKVStore data_serializer;
        data_serializer.addKeyValue("value", this->getTemperature());
        data_serializer.addKeyValue("sensor", this->getName());
        data_serializer.addKeyValue("unit", "C");
        return data_serializer;
    }

    std::string getName()
    {
        return "temperature";
    }

private:
    OneWire _oneWire;
    int _pinNumber;
    float _lastTemperature;
    DallasTemperature _sensor;
};

#endif