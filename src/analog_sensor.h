#ifndef ANALOG_SENSOR_H
#define ANALOG_SENSOR_H

#include <iostream>
#include <json_serializer.h>
#include <Arduino.h>

class AnalogSensor
{
public:
    AnalogSensor(const uint8_t pin, const std::string sensorName, const std::string unit) : _pin(pin), _name(sensorName), _unit(unit)
    {
    }

    JSONSerializeableKVStore read()
    {
        auto sensor_reading = analogRead(_pin);
        JSONSerializeableKVStore data_serializer;
        data_serializer.addKeyValue("value", sensor_reading);
        data_serializer.addKeyValue("sensor", _name);
        data_serializer.addKeyValue("unit", _unit);
        return data_serializer;
    }

    std::string getName()
    {
        return _name;
    }

private:
    uint8_t _pin;
    std::string _name;
    std::string _unit;
};

#endif // ANALOG_SENSOR_H