#include <Arduino.h>

#include <json_serializer.h>
#include <iostream>
#include <temperature_sensor.h>
#include <moving_average_filter.h>
#include <analog_sensor.h>


// Define the analog pin used to read the TMP36 sensor
MovingAverageFilter<15> temp_filter;

void setup()
{
  Serial.begin(9600);
  Serial.println("Initialized");
  pinMode(A0, INPUT);
}




void loop()
{

  auto sensor = TemperatureSensor(4);
  // auto temperature = sensor.getTemperature();
  // auto filtered_temp = sensor.calculateNewAverage(temperature);

  Serial.print(sensor.read().serialize().c_str());
  delay(1000);
}