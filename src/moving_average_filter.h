#ifndef MOVING_AVERAGE_FILTER_H
#define MOVING_AVERAGE_FILTER_H

#include <CircularBuffer.h>

template <int N>
class MovingAverageFilter
{
public:
    MovingAverageFilter() : sum(0.0){};

    float calculateNewAverage(double value)
    {
        sum += value;
        if (buffer.isFull())
        {
            sum -= buffer.pop();
        }
        buffer.unshift(value);
        return sum / buffer.size();
    }

private:
    CircularBuffer<float, N> buffer;
    float sum;
};


#endif // MOVING_AVERAGE_FILTER_H